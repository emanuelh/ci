package ci;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import model.Pokemon;


public class GameTest {
	
	@Test
	public void deve_NaoEstarMorto_QuandoVidaMaiorQueZero() {
		Pokemon pokemon = new Pokemon();
		pokemon.setLife(10);
		
		assertEquals(false, pokemon.isDead());
	}
	
	
	@Test
	public void deve_estarMorto_QuandoVidaIgualZero() {
		Pokemon pokemon = new Pokemon();
		pokemon.setLife(0);
		
		assertEquals(true, pokemon.isDead());
	}
	
	@Test
	public void deve_estarMorto_QuandoVidaMenorQueZero() {
		Pokemon pokemon = new Pokemon();
		pokemon.setLife(-1);
		
		assertEquals(true, pokemon.isDead());
	}

}
